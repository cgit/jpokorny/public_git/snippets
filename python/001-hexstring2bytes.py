# part of https://fedorapeople.org/cgit/jpokorny/public_git/snippets.git
# (c) Jan Pokorny, license determined per COPYING file in the repository's root

# we only consider absolute value (for tha case of "-0x..." hexstr parameter)
# presumably damn slow, but should work for magic strings with uknown length
hexstring2bytes = \
    lambda hexstr: \
        reduce(
            lambda acc, (i, x):
                acc[:-1] + chr(int(x + hex(ord(acc[-1]))[-1], 16)) if i % 2
                else acc + chr(int(x, 16)),
            enumerate(reversed(hexstr.lstrip('-0x'))),
            ''
        )
