# part of https://fedorapeople.org/cgit/jpokorny/public_git/snippets.git
# (c) Jan Pokorny, license determined per COPYING file in the repository's root

version_start, version_stop = reduce(
    lambda acc, x: (acc[0] if acc[0] or not x[1].isdigit() else x[0]+1,
                    acc[0] if not x[1].isdigit() else x[0]+1),
    enumerate(options.version), (0, 0)
)
if not version_start:
    options.version = '0'
else:
    options.version = options.version[version_start-1:version_stop]
